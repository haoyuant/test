﻿using System;
namespace Minutes
{
    public class Entry
    {

        public string Title;
        public string Content;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string Id = Guid.NewGuid().ToString();

        public override string ToString()
        {
            return Title + " " + CreatedDate;
        }

        public Entry()
        {
        }
    }
}
