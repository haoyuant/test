﻿using System;
namespace Minutes
{
    public static class MockDataExtensionMethods
    {
        public static void LoadMockData(this IEntryStore store) {
            var a = new Entry() { Title = "1", Content = "1. content" };
            var b = new Entry() { Title = "2", Content = "2. content" };
            var c = new Entry() { Title = "3", Content = "3. content" };
        }
    }
}
